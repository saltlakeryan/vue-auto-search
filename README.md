# vue-auto-search

> Auto search dropdown

Use like so:
```html
     <auto-search
            :records="[{'name': 'foo', 'score': 20},{'name': 'bar', 'score': 2}]"
            record-name="name"
            label="Search for User"
            minimum-search-length="1" 
            @clear-text="onClear"
            @input="onUpdate"/>
```

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
